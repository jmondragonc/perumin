jQuery(document).ready(function ($) {
  function openModal(modal) {
    $(modal).addClass("fadeIn");
  }

  function closeModal() {
    $(".modal").removeClass("fadeIn");
    $(".modal .step1").show();
    $(".modal .step2").hide();
  }

  $(".modal .close").on("click", function (e) {
    e.preventDefault();
    closeModal();
  });

  $(".btn-participar").on("click", function (e) {
    e.preventDefault();
    openModal(".participar");
  });

  // $(".link-terminos").on("click", function (e) {
  //   e.preventDefault();
  //   openModal(".terminos");
  // });

  $(".participar .btn-secondary").on("click", function (e) {
    e.preventDefault();
    $(".modal .step1").fadeOut(350, function () {
      $(".modal .step2").fadeIn(350);
    });
  });

  $(".terminos .btn-secondary").on("click", function (e) {
    e.preventDefault();
    closeModal();
  });

  $(".modal .btn-third").on("click", function (e) {
    e.preventDefault();
    $(".modal .step2").fadeOut(350, function () {
      $(".modal .step1").fadeIn(350);
    });
  });

  document.addEventListener("keydown", function (e) {
    if (e.keyCode == 27) {
      closeModal();
    }
  });
});
